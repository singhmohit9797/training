 package com.sapient.week1;

import org.junit.Assert;
import org.junit.Test;

public class PrimeNumberTest {

	@Test
	public void evenPrimeNumber() {
		Assert.assertEquals(true, PrimeNumber.isPrime(2));
	}
	
	@Test
	public void evenNotPrimeNumber() {
		Assert.assertEquals(false, PrimeNumber.isPrime(4));
	}
	
	@Test
	public void oddPrimeNumber() {
		Assert.assertEquals(true, PrimeNumber.isPrime(3));
	}
	
	
	@Test
	public void oddNotPrimeNumber() {
		Assert.assertEquals(false, PrimeNumber.isPrime(9));
	}
	
	@Test
	public void zeroTest() {
		Assert.assertEquals(false, PrimeNumber.isPrime(0));
	}
	
	@Test
	public void negativeNumber() {
		Assert.assertEquals(false, PrimeNumber.isPrime(-1));
	}
}
