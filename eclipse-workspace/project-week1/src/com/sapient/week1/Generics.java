package com.sapient.week1;

public class Generics {

	public static <E> void print(E[] elements) {
		for(E element : elements) {
			System.out.println(element);
		}
		System.out.println();
	}
	
	public static void main(String[] args) {
		Integer[] intArray = { 10, 20, 30, 40, 50 };  
        Character[] charArray = { 'T', 'E', 'S', 'T'};  
  
        System.out.println( "Printing Integer Array" );  
        print(intArray);   
 
       System.out.println( "Printing Character Array" );  
        print(charArray);   
	}
	
}
