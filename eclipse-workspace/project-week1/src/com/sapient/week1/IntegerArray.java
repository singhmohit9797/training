package com.sapient.week1;

import java.util.Arrays;

public class IntegerArray {
	
	private int size = 10;
	private int[] array;

	public IntegerArray() {
		super();
		this.array = new int[size];
	}
	
	public IntegerArray(IntegerArray integerArray) {
		this.size = integerArray.getSize();
		this.array = integerArray.getArray();
	}
	
	public IntegerArray(int size) {
		super();
		this.size = size;
		this.array = new int[size];
	}

	public IntegerArray(int[] array) {
		super();
		this.array = array;
		this.size = array.length;
	}

	public int[] getArray() {
		return array;
	}

	public void setArray(int[] array) {
		this.array = array;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}
	
	public void read() {
		for (int i = 0; i < size; i++) {
			array[i] = Read.sc.nextInt();
		}
	}
	
	public void display() {
		for(int num : array) {
			System.out.print(num+" ");
		}
	}
	
	public void sort() {
		Arrays.sort(array);
	}
	
	public void findAverage() {
		int sum = 0;
		for(int num : array) {
			sum += num;
		}
		System.out.println((float)sum/size);
	}
	
	public void search(int key) {
		sort();
		System.out.println(Arrays.binarySearch(array, key));
	}
	
	public static void main(String[] args) {
		IntegerArray ia = new IntegerArray();
		IntegerArray ia1 = new IntegerArray(new int[] {5,2,1});
		ia.read();
		System.out.println(ia1.getSize());
		ia.search(1);
		//ia1.sort();
		ia1.findAverage();
		ia.display();
	}

}
