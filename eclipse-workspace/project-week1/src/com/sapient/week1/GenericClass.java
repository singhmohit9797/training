package com.sapient.week1;

public class GenericClass<T> {
	
	private T element;

	public T getElement() {
		return element;
	}

	public void setElement(T element) {
		this.element = element;
	}
	
	public static void main(String[] args) {
		GenericClass<Integer> gint = new GenericClass<Integer>();
		gint.setElement(10);
		System.out.println(gint.getElement());
		
		GenericClass<String> gString = new GenericClass<String>();
		gString.setElement("test");
		System.out.println(gString.getElement());
	}

}
