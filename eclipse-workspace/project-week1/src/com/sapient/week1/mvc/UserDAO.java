package com.sapient.week1.mvc;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class UserDAO {
	
	public void saveUserData(UserData u) {
		
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter("data.csv", true));
			
			bw.write(u.toString());
			bw.newLine();
			bw.close();
			
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} catch (Throwable e) {
			e.printStackTrace();
		}
		System.out.println("Data saved successfully");
		
	}
	
	public void parseQuery(String query) {
		HashMap<String, Integer> map = mapColumns();

		char[] arr = query.toLowerCase().toCharArray();
		int end = 0;
		for(int i=7; i<arr.length-4; i++) {
			if(arr[i]=='f' && arr[i+1]=='r' && arr[i+2]=='o' && arr[i+3]=='m') {
				end = i;
				break;
			}
		}
		
		String fields = query.substring(7, end-1);
		String[] columns = fields.split(",");
		for(String s : columns) {
			System.out.println(s);
		}
		
		ArrayList<Integer> indices = new ArrayList<Integer>();
		
		for(int i=0; i<columns.length; i++) {
			System.out.println(map.get(columns[i]));
			indices.add(map.get(columns[i]));
			//System.out.println(element+" "+ map.get(element));
		}
		System.out.println(indices.toString());
		
		try {
			BufferedReader br = new BufferedReader(new FileReader("data.csv"));
			String line = br.readLine();
			while((line = br.readLine()) != null) {
				String[] data = line.split(",");
				for(int x : indices) {
					System.out.print(data[x]);
				}
			}
			br.close();
			
		} catch (FileNotFoundException e) {
			
			e.printStackTrace();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		
	}

	public void printAll() {
		try {
			
			BufferedReader br = new BufferedReader(new FileReader("data.csv"));
			String line;
			while((line = br.readLine()) != null) {
				System.out.println(line);
			}
			br.close();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}
	
	public HashMap<String, Integer> mapColumns(){
		HashMap<String, Integer> map = new HashMap<String, Integer>();
		try {
			BufferedReader br = new BufferedReader(new FileReader("data.csv"));
			String line = br.readLine();
			String[] columns = line.split(" ");
			/*for(String s : columns) {
				System.out.println(s);
			}*/
			br.close();
			for(int i=0; i<columns.length; i++) {
				map.put(columns[i], i);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		//System.out.println(map.values());
		return map;
	}

}
