package com.sapient.week1.mvc;

public class PlayerBean {

	private int id;
	
	private String firstName;
	
	private String lastName;
	
	private int jerseyNo;

	public PlayerBean() {
		super();
	}

	public PlayerBean(int id, String firstName, String lastName, int jerseyNo) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.jerseyNo = jerseyNo;
	}



	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getJerseyNo() {
		return jerseyNo;
	}

	public void setJerseyNo(int jerseyNo) {
		this.jerseyNo = jerseyNo;
	}

	@Override
	public String toString() {
		return "PlayerBean [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", jerseyNo=" + jerseyNo
				+ "]";
	}
	
}
