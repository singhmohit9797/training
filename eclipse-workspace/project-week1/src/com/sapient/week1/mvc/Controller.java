package com.sapient.week1.mvc;

public class Controller {
	
	public static void main(String[] args) {
		
		AddBean addBean = new AddBean();
		View view = new View();
		AddDAO addDAO = new AddDAO();
		view.readData(addBean);
		addDAO.compute(addBean);
		view.display(addBean);
		
		
		addBean = null; // dereferencing all objects for garbage collection
		view = null;
		addDAO = null;
		
		System.gc(); // this will invoke gc and finalize()
	}

}
