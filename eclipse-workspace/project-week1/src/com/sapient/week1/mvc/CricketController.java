package com.sapient.week1.mvc;

import java.util.ArrayList;
import java.util.List;

public class CricketController {
	
	public static void main(String[] args) {
		
		PlayerDAO pDAO = new PlayerDAO();
		
		//pDAO.updatePlayer(new PlayerBean(2,"Mahendra Singh","Dhoni",7));
		
		//System.out.println(pDAO.insertPlayer(new PlayerBean(2,"Mahendra","Dhoni",7)));
		
		List<PlayerBean> list = new ArrayList<PlayerBean>();
		list = pDAO.getAllPlayers();
		for(PlayerBean p : list) {
			System.out.println(p);
		}
	}

}
