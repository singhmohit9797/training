package com.sapient.week1.mvc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class PlayerDAO {

	public List<PlayerBean> getAllPlayers(){
		Connection con;
		List<PlayerBean> al = new ArrayList<PlayerBean>();
		try {
			con = DBConnect.getConnection();
			PreparedStatement ps = con.prepareStatement("select id,first_name,last_name,jersey_number from player");
			ResultSet rs = ps.executeQuery();
			PlayerBean player = null;
			while(rs.next()) {
				player = new PlayerBean(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getInt(4));
				al.add(player);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return al;
	}
	
	public PlayerBean getPlayer(int id) {
		Connection con;
		PlayerBean player = null;
		
		try {
			con = DBConnect.getConnection();
			PreparedStatement ps = con.prepareStatement("select id,first_name,last_name,jersey_number from player where id=?");
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				player = new PlayerBean(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getInt(4));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return player;
	}
	
	public int insertPlayer(PlayerBean player) {
		Connection con;
		int inserted = 0;
		try {
			con = DBConnect.getConnection();
			PreparedStatement ps = con.prepareStatement("insert into player (id,first_name,last_name,jersey_number) values (?,?,?,?)");
			ps.setInt(1, player.getId());
			ps.setString(2, player.getFirstName());
			ps.setString(3, player.getLastName());
			ps.setInt(4, player.getJerseyNo());
			inserted = ps.executeUpdate();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return inserted;
	}
	
	public int updatePlayer(PlayerBean player) {
		Connection con;
		int updatedRows = 0;
		try {
			con = DBConnect.getConnection();
			PreparedStatement ps = con.prepareStatement("update player set first_name=?,last_name=?,jersey_number=? where player.id=?");
			ps.setString(1, player.getFirstName());
			ps.setString(2, player.getLastName());
			ps.setInt(3, player.getJerseyNo());
			ps.setInt(4, player.getId());
			updatedRows = ps.executeUpdate();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return updatedRows;
	}
	
}
