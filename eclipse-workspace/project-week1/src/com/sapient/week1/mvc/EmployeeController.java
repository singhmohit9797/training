package com.sapient.week1.mvc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class EmployeeController {
	
	public static void main(String[] args) {
		
		EmployeeDAO eDAO = new EmployeeDAO();
		
		List<Employee> al = new ArrayList<Employee>();
		
		Employee e1 = new Employee();
		e1.setId(1);
		e1.setAge(25);
		e1.setName("John");
		
		Employee e2 = new Employee();
		e2.setId(2);
		e2.setAge(26);
		e2.setName("Adam");
		
		al.add(e1);
		al.add(e2);
		
		Employee emp = eDAO.getEmpById(al, 2);
		System.out.println("Search via ID: ");
		System.out.println("id: "+emp.getId()+" name: "+emp.getName()+" age: "+emp.getAge());
			
		Collections.sort(al, new NameComparator());
		System.out.println("Namewise: ");
		
		for(Employee e : al) {
			System.out.println("id: "+e.getId()+" name: "+e.getName()+" age: "+e.getAge());
		}
		
		Collections.sort(al, new AgeComparator());
		System.out.println("Agewise: ");
		for(Employee e : al) {
			System.out.println("id: "+e.getId()+" name: "+e.getName()+" age: "+e.getAge());
		}
		
	}

}
