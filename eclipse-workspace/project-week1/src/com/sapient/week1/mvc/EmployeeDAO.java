package com.sapient.week1.mvc;

import java.util.List;
import java.util.stream.Collectors;

public class EmployeeDAO {

	public Employee getEmpById(List<Employee> list, int id) {
		Employee e = list.stream()
				.filter(c -> c.getId() == id)
				.collect(Collectors.toList())
				.get(0);
		
		return e != null ? e : null;
	}
	
}
