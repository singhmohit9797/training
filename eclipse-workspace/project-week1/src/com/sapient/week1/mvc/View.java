package com.sapient.week1.mvc;

import com.sapient.week1.Read;

public class View {

	public void readData(AddBean ob) {
		System.out.println("Enter 2 numbers");
		ob.setNum1(Read.sc.nextInt());
		ob.setNum2(Read.sc.nextInt());
	}
	
	public void display(AddBean ob) {
		System.out.println("Num1 is: "+ob.getNum1());
		System.out.println("Num2 is: "+ob.getNum2());
		System.out.println("Sum is: "+ob.getSum());
	}
	
}
