package com.sapient.week1.mvc;

public class AddBean {
	
	private int num1;
	private int num2;
	private int sum;
	
	public int getSum() {
		return sum;
	}
	public void setSum(int sum) {
		this.sum = sum;
	}
	public int getNum1() {
		return num1;
	}
	public void setNum1(int num1) {
		this.num1 = num1;
	}
	public int getNum2() {
		return num2;
	}
	public void setNum2(int num2) {
		this.num2 = num2;
	}
	
	public void finalize() {
		num1 = 0;
		num2 = 0;
		sum = 0;
		System.out.println("in finalize");
	}

}
