package com.sapient.week1;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

public class LargeFile {

	public static void main(String[] args) {
		
		final long size = 4294967296L;
		System.out.println(size);
		try {
			
			//RandomAccessFile f = new RandomAccessFile("largefile.csv", "rw");
			File f = new File("largefile.csv");
			BufferedWriter bw = new BufferedWriter(new FileWriter(f));
			
			while(f.length()!=size) {
				bw.write(10);
			}
			bw.close();
				
			/*f.setLength(1024*1024*4);
			
			System.out.println(f.length());
			f.close();*/
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
