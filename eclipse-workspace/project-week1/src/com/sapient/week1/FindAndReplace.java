package com.sapient.week1;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FindAndReplace {
	
	public static void findInFile(String word) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader("D:\\Publicis.Sapient\\Training\\training\\eclipse-workspace\\project-week1\\words.txt"));
		
		// Reading file line by line
		String line = br.readLine();
		while(line != null) {
			if(line.contains(word)) // Finding word in line
				System.out.println(line);
			line = br.readLine();
		}
		br.close();
	}
	
	public static void replaceInFile(String oldWord, String newWord) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader("D:\\Publicis.Sapient\\Training\\training\\eclipse-workspace\\project-week1\\words.txt"));
		BufferedWriter bw = new BufferedWriter(new FileWriter("D:\\Publicis.Sapient\\Training\\training\\eclipse-workspace\\project-week1\\temp.txt"));
		
		String line = br.readLine();
		while(line != null) {
			if(line.contains(oldWord)) {
				line = line.replaceAll(oldWord,newWord); // Replacing oldWord with newWord
			}
			
			bw.write(line);
			bw.newLine();
			line = br.readLine();
		}
		
		br.close();
		bw.close();
		
		// Writing to temp file then copying back to original file

		BufferedWriter bw1 = new BufferedWriter(new FileWriter("D:\\Publicis.Sapient\\Training\\training\\eclipse-workspace\\project-week1\\words.txt"));
		BufferedReader br1 =  new BufferedReader(new FileReader("D:\\Publicis.Sapient\\Training\\training\\eclipse-workspace\\project-week1\\temp.txt"));
		
		String line1 = br1.readLine();
		while(line1 != null) {
			bw1.write(line1);
			bw1.newLine();
			line1 = br1.readLine();
		}
		br1.close();
		bw1.close();
		
		// Deleting the temp file
		if(new File("D:\\Publicis.Sapient\\Training\\training\\eclipse-workspace\\project-week1\\temp.txt").delete())
			System.out.println("temp deleted");
		
	}

	public static void main(String[] args) throws IOException {
		
		replaceInFile("bye", "hi");
		findInFile("hi");
		
	}
	
}
