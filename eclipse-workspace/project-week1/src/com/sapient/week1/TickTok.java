package com.sapient.week1;

class Game{
	public synchronized void g1() {
		try {
			notify();
			System.out.print("Tick - ");
			wait();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	public synchronized void g2() {
		try {
			notify();
			System.out.println("Tok");
			wait();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
}

class Player1 extends Thread{
	Game game;
	
	public Player1(Game game) {
		this.game = game;
	}
	
	public void run() {
		for(int i=0; i<10; i++) {
			game.g1();
		}
	}
}

class Player2 extends Thread{
	Game game;
	
	public Player2(Game game) {
		this.game = game;
	}
	
	public void run() {
		for(int i=0; i<10; i++) {
			game.g2();
			if(i==0)
				try {
					sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
	}
}

public class TickTok {
	
	public static void main(String[] args) {
		
		Game game = new Game();
		Player1 p1 = new Player1(game);
		Player2 p2 = new Player2(game);
		p1.start();
		p2.start();
		
	}

}
