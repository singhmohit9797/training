package com.sapient.week1;

import java.util.HashSet;
import java.util.Set;

public class RemoveDuplicates {

	public static void removeDuplicatesAndPrint(int[] arr) {
		
		Set<Integer> set = new HashSet<>();
		for(int num : arr) {
			set.add(num);
		}
		for(Integer num : set) {
			System.out.println(num);
		}			
	}
	
	public static void main(String[] args) {
		removeDuplicatesAndPrint(new int[] {1,1,2,2,3,4});
	}
	
}
