package com.sapient.week1;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class SumInEachLine {

	public static void main(String[] args) throws IOException {
		
		//Creating BufferedReader and BufferedWriter
		BufferedReader br = new BufferedReader(new FileReader("test.txt"));
		BufferedWriter bw = new BufferedWriter(new FileWriter("sum.txt"));
		
		// Reading file line by line
		String line = br.readLine();
		while(line != null) {
			String[] nums = line.split(",");
			int sum = 0;
			for(String s : nums) {
				sum += Integer.parseInt(s);
			}
			bw.write(String.valueOf(sum)); // writing sum to new file
			bw.newLine();
			line = br.readLine();
		}
		
		br.close();
		bw.close();
	}
	
}
