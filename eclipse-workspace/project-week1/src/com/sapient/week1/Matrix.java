package com.sapient.week1;

public class Matrix {
	int[][] matrix;
	
	public Matrix() {
		matrix = new int[3][3];
	}
	
	public Matrix(int m, int n) {
		matrix = new int[m][n];
	}
	
	public Matrix(int[][] mat) {
		matrix = mat;
	}
	
	public Matrix(Matrix mat) {
		this.matrix = mat.matrix;
	}
	
	public void read() {
		System.out.println("Enter the elements of matrix");
		for(int i=0; i<matrix.length; i++) {
			for(int j=0; j<matrix[i].length; j++) {
				matrix[i][j] = Read.sc.nextInt();
			}
		}
	}
	
	public void display() {
		//System.out.println("Enter the matrix of size");
		for(int i=0; i<matrix.length; i++) {
			for(int j=0; j<matrix[i].length; j++) {
				System.out.print(matrix[i][j]+" ");
			}
			System.out.println();
		}
	}
	
	public Matrix add(Matrix ob) {
		Matrix temp = new Matrix(this.matrix.length, this.matrix[0].length);
		for(int i=0; i<matrix.length; i++) {
			for(int j=0; j<matrix[i].length; j++) {
				temp.matrix[i][j] = this.matrix[i][j] + ob.matrix[i][j];
			}
		}
		return temp;
	}
	
	public Matrix multiply(Matrix ob) {
		Matrix temp = new Matrix(this.matrix.length, this.matrix[0].length);
		for(int i=0; i<matrix.length; i++) {
			for(int j=0; j<matrix[i].length; j++) {
				temp.matrix[i][j] += this.matrix[i][j] * ob.matrix[j][i];
			}
		}
		return temp;
	}
}
