package com.sapient.week2;

import java.util.List;

public class EmployeeController {
	
	public static void main(String[] args) {
		
		EmployeeDAO eDAO = new EmployeeDAO();
		List<EmployeeBean> list = eDAO.readData();
		for(EmployeeBean e : list) {
			System.out.println(e);
		}
		
		System.out.println(eDAO.getCount(list, 10000.0f));
		
		System.out.println(eDAO.getTotSal(list));
		
		System.out.println(eDAO.getEmployee(1));
	}

}
