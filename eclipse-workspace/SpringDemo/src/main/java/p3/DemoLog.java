package p3;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class DemoLog {

	private static ApplicationContext context;

	public static void main(String[] args) {
		context = new ClassPathXmlApplicationContext("bean.xml");
		Test1 t1;
		Test2 t2;
		t1 = (Test1) context.getBean("test1");
		t2 = (Test2) context.getBean("test2");
		t1.m1();
		t1.m2();
		t2.p1();
		t2.p2();
	}
	
}
