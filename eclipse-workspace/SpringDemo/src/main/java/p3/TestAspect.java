package p3;

import java.time.LocalDateTime;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.core.annotation.Order;

@Aspect
@Order(0)
public class TestAspect {

	@After("execution(* *.*())")
	public void checkm1(JoinPoint jp) {
		System.out.println(jp.getSignature() + "\t" + LocalDateTime.now());
	}
	
}
