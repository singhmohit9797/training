package p1;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class JavaContainer {

	@Bean
	@Scope("singleton")
	public Hello get1() {
		return new Hello();
	}

	@Bean
	public HolidayBean getHolidayBean() {
		return new HolidayBean("test holiday", "2/2/2020");
	}
	
	@Bean
	public HolidayList getHolidayList() {
		HolidayList hl;
		List<HolidayBean> l;
		l = new ArrayList<HolidayBean>();
		l.add(new HolidayBean("test holiday", "2/2/2020"));
		hl = new HolidayList();
		hl.setListOfHolidays(l);
		return hl;
	}
	
}
