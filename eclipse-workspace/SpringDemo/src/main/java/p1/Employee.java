package p1;

public class Employee {

	private String name;
	private int age;
	private String city;
	@Override
	public String toString() {
		return "Employee [name=" + name + ", age=" + age + "]";
	}

	public Employee() {
		super();
	}

	public Employee(String name, String city) {
		super();
		this.name = name;
		this.setCity(city);
	}
	
	public Employee(int age, String name) {
		super();
		this.name = name;
		this.age = age;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

}
