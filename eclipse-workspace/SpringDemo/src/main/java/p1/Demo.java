package p1;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Demo {
	private static ApplicationContext context;

	public static void main(String[] args) {
		context = new ClassPathXmlApplicationContext("bean.xml");
		Employee ob;
		ob = (Employee) context.getBean("employee");

		System.out.println(ob);
	}
}
