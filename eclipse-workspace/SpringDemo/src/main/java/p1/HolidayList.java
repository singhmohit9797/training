package p1;

import java.util.ArrayList;
import java.util.List;

public class HolidayList {

	private List<HolidayBean> listOfHolidays = new ArrayList<HolidayBean>();

	public HolidayList() {
		super();
		// TODO Auto-generated constructor stub
	}

	public List<HolidayBean> getListOfHolidays() {
		return listOfHolidays;
	}

	public void setListOfHolidays(List<HolidayBean> listOfHolidays) {
		this.listOfHolidays = listOfHolidays;
	}

	@Override
	public String toString() {
		return "HolidayList [listOfHolidays=" + listOfHolidays + "]";
	}

	
}
