package p1;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class JavaContainerDemo {

	private static ApplicationContext context;

	public static void main(String[] args) {
		context = new AnnotationConfigApplicationContext(JavaContainer.class);
		Hello ob;
		ob = context.getBean(Hello.class);
		ob.display();

		HolidayBean hb;
		hb = context.getBean(HolidayBean.class);
		System.out.println(hb);
		
		HolidayList hl;
		hl = context.getBean(HolidayList.class);
		System.out.println(hl);
	}

}
