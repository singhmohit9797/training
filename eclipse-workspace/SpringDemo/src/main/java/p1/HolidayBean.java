package p1;

public class HolidayBean {

	private String holidayName;

	private String holidayDate;

	public HolidayBean(String holidayName, String holidayDate) {
		super();
		this.holidayName = holidayName;
		this.holidayDate = holidayDate;
	}

	public String getHolidayName() {
		return holidayName;
	}

	public void setHolidayName(String holidayName) {
		this.holidayName = holidayName;
	}

	public String getHolidayDate() {
		return holidayDate;
	}

	public void setHolidayDate(String holidayDate) {
		this.holidayDate = holidayDate;
	}

	@Override
	public String toString() {
		return "HolidayBean [holidayName=" + holidayName + ", holidayDate=" + holidayDate + "]";
	}

}
