package p2;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.core.annotation.Order;

@Aspect
@Order(0)
public class ArithmeticAspect {

	@Before("execution(* *.*(double,double))") // @Before is Advice
	public void check1(JoinPoint jPoint) { // execution(* *.*(double,double)) is PointCut
		for (Object x : jPoint.getArgs()) {
			double v = (Double) x;
			if (v < 0) {
				throw new IllegalArgumentException("Number cannor be negative");
			}
		}
	}

	@AfterReturning(pointcut = "execution(* *.*(double,double))", returning = "val")
	public void check2(Object val) {
		double v = (Double) val;
		if (v < 0) {
			throw new IllegalArgumentException("Result cannor be negative");
		}
	}

	@AfterReturning(pointcut = "execution(* *.*(double,double))", returning = "val")
	public void check3(Object val) {
		double v = (Double) val;
		if(v > 1000) {
			throw new IllegalArgumentException("Out of Range");
		}
	}

}
