package p2;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Demo {

	private static ApplicationContext context;

	public static void main(String[] args) {
		try {
			
			context = new ClassPathXmlApplicationContext("bean.xml");
			Arithmetic ob;
			ob = (Arithmetic) context.getBean("arithmetic");

			//ob.add(-2, 3);
			//ob.subtract(-2, 3);
			ob.add(500, 600);

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

}
