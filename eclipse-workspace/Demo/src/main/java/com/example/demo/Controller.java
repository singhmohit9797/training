package com.example.demo;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

	@RequestMapping("/hello")
	public String display() {
		return "Welcome to Spring Boot Microservices";
	}
	
	@RequestMapping("/student")
	public Student displayStudent() {
		return new Student("Mohit", "22", "Gurgaon");
	}
	
}
