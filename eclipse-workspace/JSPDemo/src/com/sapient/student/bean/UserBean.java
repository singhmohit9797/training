package com.sapient.student.bean;

public class UserBean {

	private String id;
	private String username;
	private String pass;
	private String type;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	@Override
	public String toString() {
		return "UsersBean [id=" + id + ", username=" + username + ", pass=" + pass + ", type=" + type + "]";
	}
	public UserBean(String id, String username, String pass, String type) {
		super();
		this.id = id;
		this.username = username;
		this.pass = pass;
		this.type = type;
	}
	public UserBean() {
		super();
	}
	
	
}
