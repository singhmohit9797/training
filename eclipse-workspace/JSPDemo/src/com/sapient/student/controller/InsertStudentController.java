package com.sapient.student.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sapient.student.bean.StudentBean;
import com.sapient.student.bean.UserBean;
import com.sapient.student.dao.UserDAO;

/**
 * Servlet implementation class InsertStudentController
 */
public class InsertStudentController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InsertStudentController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		String name = request.getParameter("name");
		String id = request.getParameter("id");
		String rollno = request.getParameter("rollno");
		String percent = request.getParameter("percent");
		
		String password = request.getParameter("password");
		String username = request.getParameter("username");
		String type = "S";
		
		UserBean ub = new UserBean(id,username,password,type);
		StudentBean sb = new StudentBean(id,name,rollno,percent);
		
		try {
			new UserDAO().insert(ub, sb);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		RequestDispatcher rd = request.getRequestDispatcher("Insert.jsp");
		rd.forward(request, response);
		
	}

}
