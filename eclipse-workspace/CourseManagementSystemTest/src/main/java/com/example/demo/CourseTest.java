package com.example.demo;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.web.client.RestTemplate;

public class CourseTest {

	RestTemplate rest;

	static final String URL = "http://10.151.60.98:8095/";

	@Test
	public void getAllTest() {

		rest = new RestTemplate();

		String output = rest.getForObject(URL + "get/all", String.class);
		System.out.println(output);
		Assert.assertEquals(
				"[{\"id\":1,\"courseTitle\":\"Spring Boot\",\"startDate\":\"22/7/2019\",\"endDate\":\"29/7/2019\",\"fees\":\"2999\"}]",
				output);
	}

	@Test
	public void findByIdTest() {
		rest = new RestTemplate();
		String output = rest.getForObject(URL + "find/1", String.class);
		System.out.println(output);
		Assert.assertEquals(
				"{\"id\":1,\"courseTitle\":\"Spring Boot\",\"startDate\":\"22/7/2019\",\"endDate\":\"29/7/2019\",\"fees\":\"2999\"}",
				output);
	}

}
