package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CourseManagementSystemTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(CourseManagementSystemTestApplication.class, args);
	}
}
