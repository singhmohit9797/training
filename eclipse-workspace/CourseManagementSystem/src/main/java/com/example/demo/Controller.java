package com.example.demo;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

	@Autowired
	CourseDAO cDAO;

	@PostMapping("/register")
	public String newCourse(@RequestBody CourseBean course) {
		cDAO.save(course);
		return "saved course";
	}

	@GetMapping("/get/all")
	public List<CourseBean> getAllCourses() {
		return (List<CourseBean>) cDAO.findAll();
	}

	@DeleteMapping("/delete/{id}")
	public String deleteCourse(@PathVariable int id) {
		cDAO.deleteById(id);
		return "course deleted";
	}

	@GetMapping("/find/{id}")
	public Optional<CourseBean> findByID(@PathVariable int id) {
		return cDAO.findById(id);
	}

}
